package management.model;

public class Truck extends Car {
    private Long weight;
    private Long weightLoad;

    public Truck(String id, String name, Integer yearLaunch) {
        super(id, name, yearLaunch);
    }

    public Truck(String id, String name, String brand, Integer seaters, String fuel, Double price, Integer yearLaunch, Long weight, Long weightLoad) {
        super(id, name, brand, seaters, fuel, price, yearLaunch);
        this.weight = weight;
        this.weightLoad = weightLoad;
    }

    public Long getWeight() {
        return weight;
    }

    public void setWeight(Long weight) {
        this.weight = weight;
    }

    public Long getWeightLoad() {
        return weightLoad;
    }

    public void setWeightLoad(Long weightLoad) {
        this.weightLoad = weightLoad;
    }

    @Override
    public String toString() {
        String wordCars = super.toString();
        return wordCars.substring(0, wordCars.length()-1) +
                ",weight:" + weight +
                ",weightLoad:" + weightLoad +
                '}';
    }
}
