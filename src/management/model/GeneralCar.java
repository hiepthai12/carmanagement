package management.model;

public class GeneralCar extends Car {
    private String type;
    private String segment;

    public GeneralCar(String id, String name, Integer yearLaunch) {
        super(id, name, yearLaunch);
    }

    public GeneralCar(String id, String name, String brand, Integer seaters, String fuel, Double price, Integer yearLaunch, String type, String segment) {
        super(id, name, brand, seaters, fuel, price, yearLaunch);
        this.type = type;
        this.segment = segment;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getSegment() {
        return segment;
    }

    public void setSegment(String segment) {
        this.segment = segment;
    }

    @Override
    public String toString() {
        String wordCars = super.toString();

        return wordCars.substring(0, wordCars.length()-1) +
                ",\"type\":\"" + type + "\"" +
                ",\"segment\":\"" + segment + "\"" +
                '}';
    }
}
