package management.model;

public class Car {
    private String id;
    private String name;
    private String brand;
    private Integer seaters;
    private String fuel;
    private Double price;
    private Integer yearLaunch;

    public Car(String id, String name, Integer yearLaunch) {
        this.id = id;
        this.name = name;
        this.yearLaunch = yearLaunch;
    }

    public Car(String id, String name, String brand, Integer seaters, String fuel, Double price, Integer yearLaunch) {
        this.id = id;
        this.name = name;
        this.brand = brand;
        this.seaters = seaters;
        this.fuel = fuel;
        this.price = price;
        this.yearLaunch = yearLaunch;
    }

    public Integer getSeaters() {
        return seaters;
    }

    public void setSeaters(Integer seaters) {
        this.seaters = seaters;
    }

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getBrand() {
        return brand;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }

    public String getFuel() {
        return fuel;
    }

    public void setFuel(String fuel) {
        this.fuel = fuel;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Integer getYearLaunch() {
        return yearLaunch;
    }

    public void setYearLaunch(Integer yearLaunch) {
        this.yearLaunch = yearLaunch;
    }

    @Override
    public String toString() {
        return "{" +
                "\"id\":\"" + id + "\"" +
                ",\"name\":\"" + name + "\"" +
                ",\"brand\":\"" + brand + "\"" +
                ",\"seaters\":" + seaters +
                ",\"fuel\":\"" + fuel + "\"" +
                ",\"price\":" + price +
                ",\"year\":" + yearLaunch +
                "}";
    }
}
