package management.main;

import com.fasterxml.jackson.databind.ObjectMapper;
import management.common.Constant;
import management.data.FileDataProcess;
import management.enums.*;
import management.model.Car;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.*;
import java.util.stream.Collectors;

public class MainClass {
    public static void main(String[] args) {
        Map<Integer, List<Car>> carMap = FileDataProcess.getAllCar();

        List<String> fuelList = Arrays.stream(Fuel.values()).map(Fuel::getFuelType).collect(Collectors.toList());
        List<String> brandList = Arrays.stream(Brand.values()).map(Brand::getName).collect(Collectors.toList());
        List<String> generalTypeList = Arrays.stream(GeneralType.values()).map(GeneralType::getGeneralType).collect(Collectors.toList());
        List<String> segmentList = Arrays.stream(Segment.values()).map(Segment::getSegment).collect(Collectors.toList());

        List<Integer> yearList = Arrays.stream(YearLaunch.values()).map(YearLaunch::getYear).collect(Collectors.toList());

        Scanner scan = new Scanner(System.in);
        String choose;
        do {
            System.out.println(Constant.MENU_STRING);
            choose = scan.nextLine();
            switch (choose) {
                case "1":
                    List<Car> carList = new LinkedList<>();
                    yearList.forEach(year -> carList.addAll(carMap.get(year)));
                    carList.forEach(System.out::println);
                    break;
                case "2":
                    scan = new Scanner(System.in);
                    System.out.println("input brand: ");
                    String search = scan.nextLine();

                    List<Car> carByBrand = new LinkedList<>();
                    yearList.forEach(year -> {
                        List<Car> brand = carMap.get(year).parallelStream().filter(car -> car.getBrand().toLowerCase().contains(search.trim().replace(" ", "").toLowerCase())).collect(Collectors.toList());
                        carByBrand.addAll(brand);
                    });
                    carByBrand.forEach(System.out::println);
                    break;
                case "3":
                    scan = new Scanner(System.in);
                    System.out.println("id : ");
                    String id = scan.nextLine();
                    System.out.println("name : ");
                    String name = scan.nextLine();
                    System.out.println("brand : ");
                    String brand = scan.nextLine();
                    System.out.println("seaters : ");
                    Integer seater = scan.nextInt();
                    System.out.println("fuel : ");
                    String fuel = scan.nextLine();
                    System.out.println("price : ");
                    Double price = scan.nextDouble();
                    System.out.println("year launch : ");
                    Integer year = scan.nextInt();

                    Car car = new Car(id, name, brand, seater, fuel, price, year);
                    List<Car> carYear = carMap.get(year);
                    carYear.add(car);

                    //edit all car data
                    carMap.put(year, carYear);

                    //add to file year.txt
                    ObjectMapper objectMapper = new ObjectMapper();
                    File file = new File(year + Constant.TXT_FILE);
                    try {
                        FileOutputStream fileOutputStream = new FileOutputStream(file);
                        String cars = objectMapper.writeValueAsString(carYear);

                        byte[] b = cars.getBytes();
                        fileOutputStream.write(b);
                        fileOutputStream.close();
                    } catch (IOException ignored) {
                    }

                    break;
                case "4":

                    break;
                case "5":
                    break;
                case "6":
                    break;
                case "7":
                    break;
                default:
                    break;
            }
        } while (!choose.equals("7"));

    }
}
