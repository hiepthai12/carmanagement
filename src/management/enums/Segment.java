package management.enums;

public enum Segment {
    A("A", "Mini Class Vehicles"),
    B("B", "Small Class Vehicles"),
    C("C", "Sub-Medium Class Vehicles"),
    D("D", "Top-Middle Class Vehicles"),
    E("E", "Upper Class Vehicles"),
    F("F", "Luxury Class Vehicles"),
    M("M", "Multi Purpose Cars"),
    J("J", "Crossover Utility Vehicle"),
    S("S", "Super Luxury Vehicle");

    private String segment;
    private String description;

    Segment(String segment, String description) {
        this.segment = segment;
        this.description = description;
    }

    public String getSegment() {
        return segment;
    }

    public String getDescription() {
        return description;
    }
}
