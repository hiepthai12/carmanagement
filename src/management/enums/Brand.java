package management.enums;

public enum Brand {
    VINFAST(1L, "Vinfast", "Vietnam"),
    MAZDA(2L, "Mazda", "Japan"),
    TOYOTA(3L, "Toyota", "Japan"),
    MERCEDES(4L, "Mercedes", "Germany"),
    HYUNDAI(5L, "Hyundai", "Korea");

    private Long id;
    private String name;
    private String country;

    Brand(Long id, String name, String country) {
        this.id = id;
        this.name = name;
        this.country = country;
    }

    public Long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getCountry() {
        return country;
    }
}
