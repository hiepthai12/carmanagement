package management.enums;

public enum Fuel {
    GASOLINE("gasoline"), DIESEL("diesel"), ELECTRIC("electric"), HYBRID("hybrid");
    private String fuelType;

    Fuel(String fuelType) {
        this.fuelType = fuelType;
    }

    public String getFuelType() {
        return fuelType;
    }
}
