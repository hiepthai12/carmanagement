package management.enums;

public enum YearLaunch {
    Y2020(2021), Y2021(2020);

    private Integer year;

    YearLaunch(Integer year) {
        this.year = year;
    }

    public Integer getYear() {
        return year;
    }
}
