package management.enums;

public enum GeneralType {
    SEDAN("Sedan"), HATCH_BACK("Hatch Back"), SUV("Suv"), CROSS_OVER("Cross Over"), MPV("Mpv"),
    COUPE("Coupe"), CONVERTIBLE("Convertible"), PICKUP("Pickup"), LIMOUSINE("Limousine");
    private String generalType;

    GeneralType(String generalType) {
        this.generalType = generalType;
    }

    public String getGeneralType() {
        return generalType;
    }
}
