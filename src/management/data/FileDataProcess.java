package management.data;

import com.google.gson.Gson;
import management.common.Constant;
import management.enums.YearLaunch;
import management.model.Car;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.*;
import java.util.stream.Collectors;

public class FileDataProcess {
    public static Map<Integer, List<Car>> getAllCar() {
        List<Integer> yearList = Arrays.stream(YearLaunch.values()).map(YearLaunch::getYear).collect(Collectors.toList());
        Map<Integer, List<Car>> carMap = new LinkedHashMap<>();
        Gson gson = new Gson();
        yearList.forEach(year -> {
            File file = new File(year + Constant.TXT_FILE);
            try {
                BufferedReader bufferedReader = new BufferedReader(new FileReader(file));
                List<Car> carList = Arrays.stream(gson.fromJson(bufferedReader.lines().collect(Collectors.joining()), Car[].class)).collect(Collectors.toList());
                carMap.put(year, carList);
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }
        });

        return carMap;
    }
}
