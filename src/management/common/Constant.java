package management.common;

public class Constant {
    public static final String TXT_FILE = ".txt";

    public static final String MENU_STRING = "============= CAR MANAGEMENT =============\n------------------------------------------\n1. All cars information.   \n2. Find cars by a brand.   \n3. Add a new car information.    \n4. Update information of a car by id.   \n5. Remove a car information.    \n6. The most expensive car.   \n7. Exit.\n\nChoose(1 -> 7) : ";
}
